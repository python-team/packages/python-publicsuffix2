Source: python-publicsuffix2
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Scott Kitterman <scott@kitterman.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               publicsuffix,
               python3-all,
               python3-setuptools
Vcs-Git: https://salsa.debian.org/python-team/packages/python-publicsuffix2.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-publicsuffix2
Standards-Version: 4.6.0
Homepage: https://github.com/nexb/python-publicsuffix2

Package: python3-publicsuffix2
Architecture: all
Depends: publicsuffix,
         ${misc:Depends},
         ${python3:Depends}
Description: Python3 module to get a domain suffix using the Public Suffix List
 This Python3 module allows you to get the public suffix of a domain name
 using the Public Suffix List from http://publicsuffix.org.
 .
 A public suffix is one under which Internet users can directly register
 names. Some examples of public suffixes are .com, .co.uk and pvt.k12.wy.us.
 Accurately knowing the public suffix of a domain is useful when handling
 web browser cookies, highlighting the most important part of a domain name
 in a user interface or sorting URLs by web site.
 .
 This module replaces the deprecated python3-publicsuffix package.
